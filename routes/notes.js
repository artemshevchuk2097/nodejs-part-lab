const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const auth = require('../middleware/auth');

const User = require('../models/User');
const Note = require('../models/Note');

router.get('/', auth, async (req, res) => {
  const offset = req.query.offset || 0;
  const limit = req.query.limit || 10;
  try {
    const user = await User.findById(req.user.id);
    const notes = await Note.find({userId: user['_id']})
        .skip(offset).limit(limit);
    res.status(200).json({
      'offset': offset,
      'limit': limit,
      'count': notes.length,
      'notes': notes,
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.post('/', auth, async (req, res) => {
  const {text} = req.body;
  try {
    const user = await User.findById(req.user.id);
    const note = new Note({
      userId: user.id,
      text: text,
    });
    await note.save();
    res.status(200).json({message: 'Note created'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
} );

router.get('/:id', auth, async (req, res) => {
  const {id} = req.params;
  try {
    const user = await User.findById(req.user.id);
    const note = await Note.findOne({userId: user['_id'], _id: id});
    if (!note) {
      return res.status(400).json({message: 'Note not found'});
    }
    res.status(200).json({note});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
} );

router.put('/:id', auth, async (req, res) => {
  const {id} = req.params;
  const {text} = req.body;
  try {
    const user = await User.findById(req.user.id);
    const note = await Note.findOne({userId: user['_id'], _id: id});
    if (!note) {
      return res.status(400).json({message: 'Note not found'});
    }
    note.text = await text;
    await note.save();
    res.status(200).json({message: 'Note updated'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
} );

router.patch('/:id', auth, async (req, res) => {
  const {id} = req.params;
  try {
    const user = await User.findById(req.user.id);
    const note = await Note.findOne({userId: user['_id'], _id: id});
    if (!note) {
      return res.status(400).json({message: 'Note not found'});
    }
    note.completed = !note.completed;
    await note.save();
    res.status(200).json({message: 'Note updated'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
} );

router.delete('/:id', auth, async (req, res) => {
  const {id} = req.params;
  try {
    const user = await User.findById(req.user.id);
    const note = await Note.findOne({user: user['_id'], _id: id});
    if (!note) {
      return res.status(400).json({message: 'Note not found'});
    }
    await note.remove();
    res.status(200).json({message: 'Note deleted'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
} );

module.exports = router;


