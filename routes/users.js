const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const bcrypt = require('bcryptjs');
const auth = require('../middleware/auth');

const User = require('../models/User');

router.get('/me', auth, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    res.status(200).json({user});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
} );

router.delete('/me', auth, async (req, res) => {
  try {
    await User.findByIdAndDelete(req.user.id);
    res.status(200).json({message: 'User deleted'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
} );

router.patch('/me', auth, async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  try {
    if (!bcrypt.compareSync(oldPassword, req.user.password)) {
      return res.status(400).json({message: 'Incorrect old password'});
    }
    const salt = await bcrypt.genSalt(10);
    const newHashedPassword = await bcrypt.hash(newPassword, salt);
    await User.findByIdAndUpdate(req.user.id, {password: newHashedPassword});
    res.status(200).json({message: 'Password updated'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
} );

module.exports = router;
