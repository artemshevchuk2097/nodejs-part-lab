const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('config');

const User = require('../models/User');

router.post('/register', async (req, res) => {
  const {username, password} = req.body;
  try {
    let user = await User.findOne({username});
    if (user) {
      return res.status(400).json({message: 'Incorrect credentials'});
    }
    user = new User({
      username,
      password,
    });
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);
    await user.save();

    const payload = {user: {id: user.id}};
    jwt.sign(payload, config.get('jwtKey'), {expiresIn: '1h'}, (err) => {
      if (err) {
        throw err;
      }
      res.status(200).json({'message': 'User created'});
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.post('/login', async (req, res) => {
  const {username, password} = req.body;
  try {
    const user = await User.findOne({username});
    if (!user) {
      return res.status(400).json({message: 'Incorrect credentials'});
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.status(400).json({message: 'Incorrect password'});
    }
    const payload = {user: {id: user.id}};
    jwt.sign(payload, config.get('jwtKey'), {expiresIn: '1h'}, (err, token) => {
      if (err) {
        throw err;
      }
      res.status(200).json({
        'message': 'User logged in',
        'jwt_token': token,
      });
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
} );

module.exports = router;
