const express = require('express');
const app = express();
const morgan = require('morgan');
const connectDB = require('./config/db');

const PORT = process.env.PORT || 8080;

connectDB();

app.use(morgan('tiny'));
// Init middleware
app.use(express.json({extended: false}));

// Define Routes
app.use('/api/notes', require('./routes/notes'));
app.use('/api/users', require('./routes/users'));
app.use('/api/auth', require('./routes/auth'));

app.listen(PORT, () => console.log(`Server started on port: ${PORT}`));
