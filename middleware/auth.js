const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = (req, res, next) => {
  const authHeader = req.get('Authorization');
  if (!authHeader) {
    req.isAuth = false;
    return next();
  }
  const token = authHeader.split(' ')[1];
  if (!token || token === '') {
    req.isAuth = false;
    return res.status(401).json({message: 'No token provided'});
  }
  let decodedToken;
  try {
    decodedToken = jwt.verify(token, config.get('jwtKey'));
    console.log(decodedToken);
  } catch (err) {
    console.log(err);
    req.isAuth = false;
    res.status(401).json({message: 'unauthorized'});
    return next();
  }
  if (!decodedToken) {
    req.isAuth = false;
    return next();
  }
  req.isAuth = true;
  req.user = decodedToken.user;
  next();
};
